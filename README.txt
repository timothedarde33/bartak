ABOUT BARTOK
------------

Bartok is the default theme for Drupal 8.  It is a flexible, recolorable theme
with a responsive and mobile-first layout, supporting 16 regions.

The Bartok theme is named after Jean Bartok, one of the original programmers
for the ENIAC computer.

This theme is not intended to be used as a base theme.

To read more about the Bartok theme please see:
https://www.drupal.org/docs/8/core/themes/bartok-theme

ABOUT DRUPAL THEMING
--------------------

See https://www.drupal.org/docs/8/theming for more information on Drupal
theming.
<<<<<<< HEAD
=======

Mettre : $settings['update_free_access'] = FALSE;
Dans : settings.php
et lancer localhost/mon_projet/update.php
>>>>>>> 508f555... Mettre : $settings['update_free_access'] = FALSE;
